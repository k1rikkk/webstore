﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.Models
{
    public class Item : TitledModel
    {
        [DataType(DataType.Currency)]
        [Required]
        [Range(0, 999999.99)]
        public decimal Cost { get; set; }
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual List<CartItem> CartItems { get; set; }

        public override string ShortDescription => GetShortDescription(50);
    }
}
