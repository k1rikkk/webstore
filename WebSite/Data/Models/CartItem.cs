﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.Models
{
    public class CartItem : Model
    {
        public int ItemId { get; set; }
        public int CartId { get; set; }
        [Range(1, double.MaxValue, ErrorMessage = "Error: < 1")]
        public int Amount { get; set; }

        public virtual Item Item { get; set; }
        public virtual Cart Cart { get; set; }
    }
}
