﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.Models
{
    public class Cart : Model
    {
        [Required]
        public DateTime LastUpdate { get; set; }
        [Required]
        [MaxLength(100)]
        public string Address { get; set; }
        [Required]
        [MaxLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [MaxLength(150)]
        public string Info { get; set; }
        public bool IsMade { get; set; }
        public bool IsDone { get; set; }
        public bool Unproccesable { get; set; }

        public virtual List<CartItem> CartItems { get; set; }

        [NotMapped]
        public decimal TotalSum => CartItems?.Select(ci => ci.Item?.Cost * ci.Amount).Sum() ?? 0;
    }
}
