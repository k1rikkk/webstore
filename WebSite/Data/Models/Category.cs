﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.Models
{
    public class Category : TitledModel
    {
        public virtual List<Item> Items { get; set; }
    }
}
