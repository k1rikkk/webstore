﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.Models
{
    public abstract class TitledModel : Model
    {
        [Required]
        [MaxLength(30)]
        public string Title { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        [NotMapped]
        public virtual string ShortDescription => GetShortDescription(60);
        
        protected virtual string GetShortDescription(int maxLength) =>
            (Description == null) || (Description.Length <= maxLength) ? Description
            : Description.Substring(0, Description.Substring(0, maxLength).LastIndexOf(" ")) + "..";
    }
}
