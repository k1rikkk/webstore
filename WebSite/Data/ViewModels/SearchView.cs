﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Data.ViewModels
{
    public class SearchView
    {
        public IPages<Item> Pages { get; set; }
        public bool OrderByTitle { get; set; }
        public bool OrderByCost { get; set; }
        public string Query { get; set; }
    }
}
