﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Data.ViewModels
{
    public class StatisticsView
    {
        public List<StatisticItem> TopCategories { get; }
        public List<StatisticItem> TopItems { get; }
        public List<StatisticItem> MonthlyCartsTotalSum { get; }

        protected List<StatisticItem> GetTop(Dictionary<TitledModel, int> raw)
        {
            List<StatisticItem> results = new List<StatisticItem>();
            float total = raw.Select(r => r.Value).Sum();
            int current = 0;
            int step = 360 / raw.Count;
            for (int i = 0; i < raw.Count - 1; i++)
            {
                results.Add(new StatisticItem($"{raw.ElementAt(i).Key.Title} "
                    + $"{(100 * raw.ElementAt(i).Value / total).ToString("0.00")}%"
                    , raw.ElementAt(i).Value
                    , current, 100, 70));
                current += step;
            }
            results.Add(new StatisticItem($"Others {(100 * raw.Last().Value / total).ToString("0.00")}%"
                , raw.Last().Value, current, 100, 70));
            return results;
        }

        public StatisticsView(Dictionary<TitledModel, int> rawItems, Dictionary<TitledModel, int> rawCategories
            , Dictionary<string, decimal> rawMonthlySum)
        {
            TopItems = GetTop(rawItems);
            TopCategories = GetTop(rawCategories);
            MonthlyCartsTotalSum = GetMonthlyCartSum(rawMonthlySum);
        }

        protected List<StatisticItem> GetMonthlyCartSum(Dictionary<string, decimal> raw)
            => raw.Select(kv => new StatisticItem(kv.Key, kv.Value, 150, 100, 70)).ToList();

        public class StatisticItem
        {
            public string Title { get; set; }
            public decimal Data { get; set; }
            public int H { get; set; }
            public int S { get; set; }
            public int L { get; set; }

            public StatisticItem(string title, decimal data, int h, int s, int l)
            {
                Title = title;
                Data = data;
                H = h;
                S = s;
                L = l;
            }
        }
    }
}
