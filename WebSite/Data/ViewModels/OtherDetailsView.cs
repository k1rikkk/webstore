﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Data.ViewModels
{
    public class OtherDetailsView<T> where T: Model
    {
        public T Model { get; set; }
        public List<T> Other { get; set; }
    }
}
