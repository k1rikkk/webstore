﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.ViewModels
{
    public class ChartView
    {
        public string Type { get; set; }
        public string CanvasId { get; set; }
        public List<StatisticsView.StatisticItem> Items { get; set; }

        public ChartView(string type, string canvasId, List<StatisticsView.StatisticItem> items)
        {
            Type = type;
            CanvasId = canvasId;
            Items = items;
        }
    }
}
