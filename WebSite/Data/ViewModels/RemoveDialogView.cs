﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Data.ViewModels
{
    public class RemoveDialogView
    {
        public string FormAction { get; set; }
        public string BodyText { get; set; }
    }
}
