﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Data.ViewModels
{
    public class PagedDetailsView<M, T> where M: Model where T: Model
    {
        public M Model { get; set; }
        public IPages<T> Pages { get; set; }
    }
}
