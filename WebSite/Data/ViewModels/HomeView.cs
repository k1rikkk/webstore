﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Data.ViewModels
{
    public class HomeView
    {
        public List<Category> Categories { get; set; }
        public List<Item> MostRated { get; set; }
        public List<Category> TopCategories { get; set; }
    }
}
