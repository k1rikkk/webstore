﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Data.ViewModels
{
    public class CartsBrowserView
    {
        public List<Cart> Carts { get; set; }
        public bool AutoReload { get; set; }
        public int ReloadInterval { get; set; }
        public bool ShowDone { get; set; }
        public bool ShowUnprocessable { get; set; }
    }
}
