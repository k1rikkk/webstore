﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Services.NonInjectable;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite
{
    public static partial class Extensions
    {
        public static IWebHostBuilder AutoConfigureLogging(this IWebHostBuilder host)
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .Build();
            host.ConfigureLogging((logging) =>
            {
                logging.AddConsole();
                logging.AddSerilog(new LoggerConfiguration()
                    .MinimumLevel.Warning()
                    .WriteTo.File(config.GetValue<string>("Log path"))
                    .CreateLogger(), true);
            });
            return host;
        }

        public static IWebHost UseCartsCleaner(this IWebHost host)
        {
            new CartsCleaner(host).Run();
            return host;
        }

        public static IWebHostBuilder UseUrlFromConfig(this IWebHostBuilder host)
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .Build();
            host.UseUrls(config.GetValue<string>("Url"));
            return host;
        }

        public static IWebHost CheckDb(this IWebHost host)
        {
            using (IServiceScope scope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                AppDbContext dbContext = scope.ServiceProvider.GetService<AppDbContext>();
                dbContext.Database.EnsureCreatedAsync().Wait();
                if (scope.ServiceProvider.GetService<IConfiguration>().GetValue("Seed", false))
                    SeedDb(dbContext).Wait();
            }
            return host;
        }

        public static async Task SeedDb(AppDbContext dbContext)
        {
            if ((await dbContext.Categories.CountAsync() != 0) || (await dbContext.Items.CountAsync() != 0))
                return;
            List<Category> categories = new List<Category>();
            List<Item> items = new List<Item>();
            List<Cart> carts = new List<Cart>();
            List<CartItem> cartItems = new List<CartItem>();

            categories.Add(new Category
            {
                Title = "Soft drinks",
                Description = "Lorem ipsum eros tempus sagittis integer, leo aliquam pretium curabitur arcu "
                    + "aenean, dapibus elit blandit augue scelerisque rhoncus urna habitant mollis imperdiet risus id.",
                ImageUrl = "1.png"
            });
            categories.Add(new Category
            {
                Title = "Desserts",
                Description = "Lorem ipsum phasellus suspendisse lacus tristique dapibus adipiscing in, tempus "
                    + "potenti facilisis dictumst sollicitudin aliquam aenean, cras orci scelerisque nunc proin "
                    + "commodo interdum rutrum conubia metus tincidunt bibendum senectus.",
                ImageUrl = "2.png"
            });
            categories.Add(new Category
            {
                Title = "Hot meals",
                Description = "Lorem ipsum eros tempus sagittis integer, leo aliquam pretium curabitur arcu "
                    + "aenean, dapibus elit blandit consequat augue scelerisque rhoncus urna habitant eget mollis "
                    + "imperdiet ut risus id.",
                ImageUrl = "3.png"
            });
            categories.Add(new Category
            {
                Title = "Pizzas",
                Description = "Lorem ipsum eros tempus sagittis integer, leo aliquam pretium curabitur arcu "
                    + "aenean, dapibus elit blandit consequat augue scelerisque rhoncus urna habitant eget mollis "
                    + "imperdiet ut risus id.",
                ImageUrl = "4.png"
            });
            //  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
            #region Soft drinks
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 0.99m,
                Title = "Coca-cola",
                Description = "500 ml",
                ImageUrl = "1.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 0.99m,
                Title = "Sprite",
                Description = "500 ml",
                ImageUrl = "2.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.29m,
                Title = "Fanta",
                Description = "Orange, 500 ml",
                ImageUrl = "5.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.29m,
                Title = "Fanta",
                Description = "Elderberry-lemon, 500 ml",
                ImageUrl = "6.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.32m,
                Title = "Lipton",
                Description = "Lime & mint, 500 ml",
                ImageUrl = "7.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.32m,
                Title = "Lipton",
                Description = "Mango, 500 ml",
                ImageUrl = "8.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.32m,
                Title = "Lipton",
                Description = "Raspberry, 500 ml",
                ImageUrl = "9.png"
            });
            items.Add(new Item
            {
                Category = categories[0],
                Cost = 1.16m,
                Title = "7up",
                Description = "500 ml",
                ImageUrl = "10.png"
            });
            #endregion
            #region Desserts
            items.Add(new Item
            {
                Category = categories[1],
                Cost = 1.39m,
                Title = "Ice cream",
                Description = "100 g, with chocolate",
                ImageUrl = "3.png"
            });
            items.Add(new Item
            {
                Category = categories[1],
                Cost = 3.17m,
                Title = "Cranachan",
                Description = "Scottish Whipped Cream With Whisky, Raspberries, and Toasted",
                ImageUrl = "11.png"
            });
            items.Add(new Item
            {
                Category = categories[1],
                Cost = 2.53m,
                Title = "Chocolate peanut dessert",
                Description = "Cool and creamy, oreo, peanut butter and chocolate loaded dessert",
                ImageUrl = "12.png"
            });
            items.Add(new Item
            {
                Category = categories[1],
                Cost = 2.33m,
                Title = "Cranberry pretzel salad",
                Description = "Both sweet and savory, this Cranberry Pretzel Salad offers the best of two worlds",
                ImageUrl = "13.png"
            });
            items.Add(new Item
            {
                Category = categories[1],
                Cost = 2.91m,
                Title = "White chocolate crunch",
                Description = "Simple & tasty dessert finished with chocolate chip cookie crumbs",
                ImageUrl = "14.png"
            });
            #endregion
            #region Hot meals
            items.Add(new Item
            {
                Category = categories[2],
                Cost = 3.19m,
                Title = "Steak",
                Description = "150 g",
                ImageUrl = "4.png"
            });
            items.Add(new Item
            {
                Category = categories[2],
                Cost = 4.14m,
                Title = "Crispy fried chicken",
                Description = "200 g",
                ImageUrl = "15.png"
            });
            #endregion
            #region Pizzas
            items.Add(new Item
            {
                Category = categories[3],
                Cost = 5.84m,
                Title = "Vegetariana",
                Description = "Tomatoes, mushrooms, onions, bell peppers, black olives and mozzarella cheese",
                ImageUrl = "16.png"
            });
            items.Add(new Item
            {
                Category = categories[3],
                Cost = 6.29m,
                Title = "Bacon & tuna",
                Description = "Tomato sauce, mozzarella cheese, bacon and tuna",
                ImageUrl = "17.png"
            });
            items.Add(new Item
            {
                Category = categories[3],
                Cost = 5.01m,
                Title = "Pepperoni",
                Description = "Mozzarella cheese, cream cheese, pepper, 16 slices of pepperoni and sprinkled oregano",
                ImageUrl = "18.png"
            });
            items.Add(new Item
            {
                Category = categories[3],
                Cost = 6.95m,
                Title = "Four seasons",
                Description = "Delicious pizza recipe, based on famous Italian recipe Quattro Stagioni",
                ImageUrl = "19.png"
            });
            #endregion
            //  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
            for (int c = 0; c < 12; c++)
            {
                Cart cart = new Cart
                {
                    Address = "unset",
                    PhoneNumber = "unset",
                    IsMade = true,
                    IsDone = true,
                    LastUpdate = DateTime.Now - new TimeSpan(new Random().Next(0, 180), 0, 0, 0)
                };
                carts.Add(cart);
                int count = new Random().Next(1, 5);
                for (int i = 0; i < count; i++)
                    cartItems.Add(new CartItem
                    {
                        Cart = cart,
                        Amount = new Random().Next(1, 4),
                        Item = items.ElementAt(new Random().Next(0, items.Count))
                    });
            }
            await dbContext.Categories.AddRangeAsync(categories);
            await dbContext.Items.AddRangeAsync(items);
            await dbContext.Carts.AddRangeAsync(carts);
            await dbContext.CartItems.AddRangeAsync(cartItems);
            await dbContext.SaveChangesAsync();
        }
    }
}
