﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebSite.Data;
using WebSite.Services;
using WebSite.Services.Interfaces;

namespace WebSite
{
    public class Startup
    {
        protected IConfiguration configuration;

        public Startup(IConfiguration configuration) => this.configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMemoryCache();
            services.AddSession();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddDbContext<AppDbContext>(options 
                => options.UseSqlite(configuration.GetValue<string>("Sqlite connection string")));
            services.AddScoped<ICartProvider, CartProvider>();
            services.AddScoped<INavDataProvider, NavDataProvider>();
            services.AddScoped<IAnalyst, Analyst>();
            services.AddScoped<IImagesUploader, ImagesUploader>();
            services.AddScoped<IUpdateProcessor, UpdateProcessor>();
            services.AddMvc();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Management/Error");
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            app.UseMvcWithDefaultRoute();
        }
    }
}
