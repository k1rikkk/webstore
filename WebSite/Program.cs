﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace WebSite
{
    public class Program
    {
        public static void Main(string[] args) 
            => BuildWebHost(args)
                .CheckDb()
                .UseCartsCleaner()
                .Run();

        public static IWebHost BuildWebHost(string[] args) 
            => WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                //.UseUrlFromConfig() 
                //.UseUrls("http://192.168.0.11:5000")
                .UseStartup<Startup>()
                .AutoConfigureLogging()
                .Build();
    }
}
