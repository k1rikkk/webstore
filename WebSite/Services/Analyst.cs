﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Services.Interfaces;

namespace WebSite.Services
{
    public class Analyst : IAnalyst
    {
        protected AppDbContext dbContext;

        public Analyst(AppDbContext dbContext) => this.dbContext = dbContext;

        public virtual async Task<List<Category>> GetTopCategoriesIgnoreAmountAsync(int maxCount = 1)
            => await dbContext.Items.OrderByDescending(i => i.CartItems.Count(ci => ci.Cart.IsMade == true))
                   .GroupBy(i => i.Category).OrderByDescending(gp => gp.Sum(i => i.CartItems.Count(ci => ci.Cart.IsMade == true)))
                   .Select(gp => gp.Key).Take(maxCount).ToListAsync();

        public virtual async Task<List<Item>> GetTopItemsIgnoreAmountAsync(int maxCount = 1)
            => await dbContext.Items.OrderByDescending(i => i.CartItems.Count(ci => ci.Cart.IsMade == true))
                .Take(maxCount).ToListAsync();

        public virtual async Task<Dictionary<TitledModel, int>> GetTopItemsAsync(int maxCount = 3)
        {
            List<TitledModel> items = await dbContext.Items.OrderByDescending(i => i.CartItems
                .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                .Sum(ci => ci.Amount)).Take(maxCount).ToAsyncEnumerable().Select(i => (TitledModel)i).ToList();
            List<int> counts = await dbContext.Items
                .Select(i => i.CartItems
                    .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                    .Sum(ci => ci.Amount))
                .OrderByDescending(s => s).Take(maxCount).ToListAsync();
            int total = await dbContext.Items.Select(i => i.CartItems
                .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                .Sum(ci => ci.Amount)).SumAsync();
            counts.Add(total - counts.Sum());
            Dictionary<TitledModel, int> results = new Dictionary<TitledModel, int>();
            for (int i = 0; i < items.Count; i++)
                results.Add(items[i], counts[i]);
            results.Add(new Item(), counts.Last());
            return results;
        }

        public virtual async Task<Dictionary<TitledModel, int>> GetTopCategoriesAsync(int maxCount = 3)
        {
            List<TitledModel> categories = await dbContext.Categories.OrderByDescending(c => c.Items.Sum(i => i.CartItems
                .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                .Sum(ci => ci.Amount))).Take(maxCount).ToAsyncEnumerable().Select(i => (TitledModel)i).ToList();
            List<int> counts = await dbContext.Categories
                .Select(c => c.Items.Sum(i => i.CartItems
                    .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                    .Sum(ci => ci.Amount)))
                .OrderByDescending(s => s).Take(maxCount).ToListAsync();
            int total = await dbContext.Items.Select(i => i.CartItems
                .Where(ci => (DateTime.Now - ci.Cart.LastUpdate < new TimeSpan(30, 0, 0, 0)) && (ci.Cart.IsDone))
                .Sum(ci => ci.Amount)).SumAsync();
            counts.Add(total - counts.Sum());
            Dictionary<TitledModel, int> results = new Dictionary<TitledModel, int>();
            for (int i = 0; i < categories.Count; i++)
                results.Add(categories[i], counts[i]);
            results.Add(new Category(), counts.Last());
            return results;
        }

        public virtual async Task<Dictionary<string, decimal>> GetMonthlyTotalCartsSum(int monthCount = 6)
        {
            Dictionary<string, decimal> results = new Dictionary<string, decimal>(monthCount);
            DateTime dateTime = GetMonthStart(DateTime.Now);
            for (int i = 0; i < monthCount; i++)
                dateTime = GetMonthStart(dateTime - new TimeSpan(3, 0, 0, 0));
            for (int i = 0; i < monthCount; i++)
            {
                DateTime next = GetMonthStart(dateTime + new TimeSpan(33, 0, 0, 0));
                decimal result = await dbContext.Carts.Where(c => (c.LastUpdate >= dateTime) && (c.LastUpdate < next)
                    && (c.IsDone)).Select(c => c.CartItems.Select(ci => ci.Amount * ci.Item.Cost).Sum()).SumAsync();
                results.Add(dateTime.ToString("MM/yyyy"), result);
                dateTime = next;
            }
            return results;
        }

        protected DateTime GetMonthStart(DateTime dateTime) => new DateTime(dateTime.Year, dateTime.Month, 1);
    }
}
