﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Services.NonInjectable
{
    public class PagesBuilder
    {
        public int Current { get; protected set; }
        public int ItemsPerPage { get; }
        public bool NormalizePage { get; }
        public string Path { get; }
        
        /// <param name="path">{page} will be replaced with page</param>
        public PagesBuilder(string path, int current, int itemsPerPage, bool normalizePage = false)
        {
            Path = path;
            Current = current;
            ItemsPerPage = itemsPerPage;
            NormalizePage = normalizePage;
        }

        public virtual async Task<IPages<T>> Build<T, F>(IQueryable<T> dbSet) where T: Model where F: PagesGenerator, new()
        {
            int total = (int)Math.Ceiling(await dbSet.CountAsync() / (float)ItemsPerPage);
            Current = NormalizePage ? ((Current > 0 ? Current : 1) > total ? total : Current) : Current;
            List<T> items = await dbSet.Skip((Current - 1) * ItemsPerPage).Take(ItemsPerPage).ToListAsync();
            return new Pages<T>(Path, Current, total, ItemsPerPage, items, new F());
        }

        protected class Pages<T> : IPages<T> where T: Model
        {
            public int Current { get; }
            public int Total { get; }
            public int ItemsPerPage { get; }
            public string Path { get; }
            public List<T> Items { get; }
            public PagesGenerator PagesGenerator { get; }

            public Pages(string path, int current, int total, int itemsPerPage, List<T> items, PagesGenerator pagesGenerator)
            {
                Path = path;
                Current = current;
                Total = total;
                ItemsPerPage = itemsPerPage;
                Items = items;
                PagesGenerator = pagesGenerator;
                PagesGenerator.PagesRaw = this;
            }

            public string GetHtml(int page) => Path.Replace("{page}", page.ToString());
        }
    }
}
