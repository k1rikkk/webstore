﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Services.NonInjectable
{
    public class TwoClosestPagesGenerator : PagesGenerator
    {
        public override IEnumerable<int> PrePages
        {
            get
            {
                for (int i = Math.Max(PagesRaw.Current - 2, 2); i < PagesRaw.Current; i++)
                    yield return i;
            }
        }

        public override IEnumerable<int> PostPages
        {
            get
            {
                for (int i = PagesRaw.Current + 1; (i < PagesRaw.Total) && (i - PagesRaw.Current < 3); i++)
                    yield return i;
            }
        }

        public override bool ShowPreDots => PagesRaw.Current > 4;
        public override bool ShowPostDots => PagesRaw.Current + 3 < PagesRaw.Total;
    }
}
