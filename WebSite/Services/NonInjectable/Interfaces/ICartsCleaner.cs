﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Services.NonInjectable.Interfaces
{
    public interface ICartsCleaner
    {
        void Run();
    }
}
