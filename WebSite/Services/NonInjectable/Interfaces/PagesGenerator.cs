﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Services.NonInjectable.Interfaces
{
    public abstract class PagesGenerator
    {
        public PagesGenerator() { }
        public virtual IPagesRaw PagesRaw { get; set; }
        public abstract IEnumerable<int> PrePages { get; }
        public abstract IEnumerable<int> PostPages { get; }
        public abstract bool ShowPreDots { get; }
        public abstract bool ShowPostDots { get; }
    }
}
