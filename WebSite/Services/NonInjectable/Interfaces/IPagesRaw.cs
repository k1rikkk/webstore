﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Services.NonInjectable.Interfaces
{
    public interface IPagesRaw
    {
        int Current { get; }
        int Total { get; }
        int ItemsPerPage { get; }
        string Path { get; }
        PagesGenerator PagesGenerator { get; }
        string GetHtml(int page);
    }
}
