﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Services.NonInjectable.Interfaces
{
    public interface IPages<T> : IPagesRaw where T: Model
    {
        List<T> Items { get; }
    }
}
