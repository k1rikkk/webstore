﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Services.NonInjectable
{
    public class CartsCleaner : ICartsCleaner
    {
        protected IWebHost host;
        protected int interval;

        public CartsCleaner(IWebHost host)
        {
            this.host = host;
            interval = host.Services.GetService<IConfiguration>().GetValue<int>("Carts cleaner interval");
        }

        public void Run()
        {
            Task.Run(new Action(async () =>
            {
                while (true)
                {
                    using (IServiceScope scope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
                    {
                        AppDbContext dbContext = scope.ServiceProvider.GetService<AppDbContext>();
                        dbContext.RemoveRange(await dbContext.Carts.Where(c 
                            => (c.IsMade == false) && (DateTime.Now - c.LastUpdate > new TimeSpan(0, 0, interval)))
                            .ToListAsync());
                        await dbContext.SaveChangesAsync();
                    }
                    await Task.Delay(interval * 1000);
                }
            }));
        }
    }
}
