﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using WebSite.Data.Models;
using WebSite.Services.Interfaces;

namespace WebSite.Services
{
    public class ImagesUploader : IImagesUploader
    {
        protected INavDataProvider navData;
        protected string webRootPath;

        public ImagesUploader(INavDataProvider navData, IHostingEnvironment environment)
        {
            this.navData = navData;
            webRootPath = environment.WebRootPath;
        }

        public async Task UploadCategoryAsync(IFormFile file, string fileName) 
            => await UploadAsync(file, webRootPath + navData.CategoriesDirectory + fileName);

        public void RemoveCategory(string fileName) => File.Delete(webRootPath + navData.CategoriesDirectory + fileName);

        public async Task UploadItemAsync(IFormFile file, string fileName) 
            => await UploadAsync(file, webRootPath + navData.ItemsDirectory + fileName);

        protected async Task UploadAsync(IFormFile file, string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
                await file.CopyToAsync(stream);
        }

        public void RemoveItem(string fileName) => File.Delete(webRootPath + navData.ItemsDirectory + fileName);
    }
}
