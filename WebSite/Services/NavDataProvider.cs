﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Services.Interfaces;

namespace WebSite.Services
{
    public class NavDataProvider : INavDataProvider
    {
        protected AppDbContext dbContext;
        protected IConfiguration configuration;
        protected HttpContext httpContext;
        protected string[] pathFragments;

        public NavDataProvider(AppDbContext dbContext, IConfiguration configuration, IHttpContextAccessor contextAccessor)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
            httpContext = contextAccessor.HttpContext;
            pathFragments = httpContext.Request.Path.Value.Split('/', StringSplitOptions.RemoveEmptyEntries);
        }

        public virtual IEnumerable<Category> Categories => 
            dbContext.Categories.OrderBy(c => c.Title).ToListAsync().GetAwaiter().GetResult();

        public virtual string ItemsDirectory => configuration.GetSection("Images directories").GetValue<string>("Items");

        public virtual string CategoriesDirectory => configuration.GetSection("Images directories").GetValue<string>("Categories");

        public string Controller => pathFragments.ElementAtOrDefault(0);

        public string Action => pathFragments.ElementAtOrDefault(1);

        public string Id => pathFragments.ElementAtOrDefault(2);
    }
}
