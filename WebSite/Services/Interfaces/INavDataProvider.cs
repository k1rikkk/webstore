﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Services.Interfaces
{
    public interface INavDataProvider
    {
        string ItemsDirectory { get; }
        string CategoriesDirectory { get; }
        IEnumerable<Category> Categories { get; }
        string Controller { get; }
        string Action { get; }
        string Id { get; }
    }
}
