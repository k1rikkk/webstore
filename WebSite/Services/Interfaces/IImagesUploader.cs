﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Services.Interfaces
{
    public interface IImagesUploader
    {
        Task UploadCategoryAsync(IFormFile file, string fileName);
        void RemoveCategory(string fileName);
        Task UploadItemAsync(IFormFile file, string fileName);
        void RemoveItem(string fileName);
    }
}
