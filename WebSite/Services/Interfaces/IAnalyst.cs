﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Services.Interfaces
{
    public interface IAnalyst
    {
        Task<List<Item>> GetTopItemsIgnoreAmountAsync(int maxCount = 1);
        Task<List<Category>> GetTopCategoriesIgnoreAmountAsync(int maxCount = 1);
        Task<Dictionary<TitledModel, int>> GetTopItemsAsync(int maxCount = 3);
        Task<Dictionary<TitledModel, int>> GetTopCategoriesAsync(int maxCount = 3);
        Task<Dictionary<string, decimal>> GetMonthlyTotalCartsSum(int monthCount = 6);
    }
}
