﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Services.Interfaces
{
    public interface ICartProvider
    {
        Task<Cart> GetCurrent();
        Task Add(int itemId, int amount = 1);
        Task Remove(int itemId);
        Task<decimal> GetSum();
        Task Reset();
    }
}
