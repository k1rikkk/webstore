﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data.Models;

namespace WebSite.Services.Interfaces
{
    public interface IUpdateProcessor
    {
        Task UpdateTitled(TitledModel model, IFormFile image);
        Task AddTitled(TitledModel model, IFormFile image);
        Task RemoveTitled(TitledModel model);
    }
}
