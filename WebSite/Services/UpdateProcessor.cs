﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Services.Interfaces;

namespace WebSite.Services
{
    public class UpdateProcessor : IUpdateProcessor
    {
        protected IImagesUploader imagesUploader;
        protected AppDbContext dbContext;

        public UpdateProcessor(IImagesUploader imagesUploader, AppDbContext dbContext)
        {
            this.imagesUploader = imagesUploader;
            this.dbContext = dbContext;
        }

        public async Task AddTitled(TitledModel model, IFormFile image)
        {
            await dbContext.AddAsync(model);
            await dbContext.SaveChangesAsync();
            string fileName = model.Id + Path.GetExtension(image.FileName);
            if (model.GetType() == typeof(Item))
                await imagesUploader.UploadItemAsync(image, fileName);
            else
                await imagesUploader.UploadCategoryAsync(image, fileName);
            model.ImageUrl = fileName;
            dbContext.Update(model);
            await dbContext.SaveChangesAsync();
        }

        public async Task RemoveTitled(TitledModel model)
        {
            string fileName = model.ImageUrl;
            if (fileName != null)
            {
                if (model.GetType() == typeof(Item))
                    imagesUploader.RemoveItem(fileName);
                else
                    imagesUploader.RemoveCategory(fileName);
            }
            dbContext.Remove(model);
            await dbContext.SaveChangesAsync();
        }

        public async Task UpdateTitled(TitledModel model, IFormFile image)
        {
            if (image != null)
            {
                string fileName = model.Id + Path.GetExtension(image.FileName);
                if (model.GetType() == typeof(Item))
                    await imagesUploader.UploadItemAsync(image, fileName);
                else
                    await imagesUploader.UploadCategoryAsync(image, fileName);
                model.ImageUrl = fileName;
            }
            dbContext.Update(model);
            await dbContext.SaveChangesAsync();
        }
    }
}
