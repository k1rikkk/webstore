﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Services.Interfaces;

namespace WebSite.Services
{
    public class CartProvider : ICartProvider
    {
        protected AppDbContext dbContext;
        protected HttpContext httpContext;
        
        public CartProvider(AppDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            this.dbContext = dbContext;
            httpContext = httpContextAccessor.HttpContext;
        }

        public virtual async Task<Cart> GetCurrent()
        {
            int? id = httpContext.Session.GetInt32("CartId");
            if (id == null)
                return await CreateCurrent();
            return await dbContext.Carts.Where(c => c.Id == id.Value).Include(c => c.CartItems)
                .ThenInclude(ci => ci.Item).FirstOrDefaultAsync() ?? await CreateCurrent();
        }

        protected virtual async Task<Cart> CreateCurrent()
        {
            Cart cart = new Cart { Address = "Empty", PhoneNumber = "Empty", LastUpdate = DateTime.Now, CartItems = new List<CartItem>() };
            await dbContext.AddAsync(cart);
            await dbContext.SaveChangesAsync();
            httpContext.Session.SetInt32("CartId", cart.Id);
            return cart;
        }

        protected virtual async Task Update(Cart cart)
        {
            cart.LastUpdate = DateTime.Now;
            await dbContext.SaveChangesAsync();
        }

        public virtual async Task Add(int itemId, int amount = 1)
        {
            Cart current = await GetCurrent();
            CartItem oldCartItem = current.CartItems.FirstOrDefault(ci => ci.ItemId == itemId);
            if (oldCartItem == null)
                current.CartItems.Add(new CartItem { ItemId = itemId, Amount = amount });
            else
                oldCartItem.Amount += amount;
            await Update(current);
        }

        public virtual async Task Remove(int itemId)
        {
            Cart current = await GetCurrent();
            current.CartItems.Remove(current.CartItems.FirstOrDefault(ci => ci.ItemId == itemId));
            await Update(current);
        }

        public async Task<decimal> GetSum() 
            => (await GetCurrent()).TotalSum;

        public async Task Reset() => await CreateCurrent();
    }
}
