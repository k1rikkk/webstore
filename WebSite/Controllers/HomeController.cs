﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Data.ViewModels;
using WebSite.Services;
using WebSite.Services.Interfaces;
using WebSite.Services.NonInjectable;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        protected AppDbContext dbContext;
        protected IAnalyst analyst;
        protected int searchItemsPerPage = 8;
        protected int mostRatedCount = 4;
        protected int topCategoriesCount = 4;
        protected string adminKey;
        protected string operatorKey;

        public HomeController(AppDbContext dbContext, IAnalyst analyst, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.analyst = analyst;
            adminKey = configuration.GetValue<string>("Admin key");
            operatorKey = configuration.GetValue<string>("Operator key");
        }

        public async Task<IActionResult> Index() => View(new HomeView
        {
            Categories = await dbContext.Categories.ToListAsync(),
            MostRated = await analyst.GetTopItemsIgnoreAmountAsync(mostRatedCount),
            TopCategories = await analyst.GetTopCategoriesIgnoreAmountAsync(topCategoriesCount)
        });

        protected virtual string RemoveSeparators(string data)
        {
            string output = "";
            for (int i = 0; i < data.Length; i++)
            {
                if (char.IsLetterOrDigit(data[i]) || char.IsWhiteSpace(data[i]))
                    output += data[i];
            }
            return output;
        }

        public async Task<IActionResult> Search(string query, int sort = 0, int page = 1)
        {
            bool orderByTitle = sort == 0;
            bool orderByCost = sort == 1;
            query = RemoveSeparators(query == null ? "" : query);
            string[] tags = query.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            IQueryable<Item> queryable = dbContext.Items.FromSql(GetSearchSql(tags, orderByTitle, orderByCost));
            IPages<Item> pages = await new PagesBuilder("/Home/Search?query=" + query + "&page={page}"
                + $"&orderByName={orderByTitle}&orderByCost={orderByCost}", page, searchItemsPerPage, true)
                .Build<Item, TwoClosestPagesGenerator>(queryable);
            ViewBag.Query = query;
            SearchView view = new SearchView
            {
                Pages = pages,
                OrderByCost = orderByCost,
                OrderByTitle = orderByTitle,
                Query = query
            };
            return View(view);
        }

        protected string GetSearchSql(string[] tags, bool orderByTitle, bool orderByCost)
        {
            string orderString = $"{(orderByTitle || orderByCost ? " ORDER BY " : "")} {(orderByTitle? "Title" : "Cost")}";
            if (tags.Length == 0)
                return $"SELECT * FROM Items {orderString}";
            string sql = $"SELECT Items.* FROM Items, Categories WHERE Items.CategoryId = Categories.Id ";
            string titleSql = $"AND ((Items.Title LIKE \'%{tags.First()}%\' ";
            string descriptionSql = $") OR (Items.Description LIKE \'%{tags.First()}%\' ";
            string categoryTitleSql = $") OR (Categories.Title LIKE \'%{tags.First()}%\' ";
            string categoryDescriptionSql = $") OR (Categories.Description LIKE \'%{tags.First()}%\' ";
            for (int i = 1; i < tags.Length; i++)
            {
                titleSql += $"AND Items.Title LIKE \'%{tags[i]}%\' ";
                descriptionSql += $"AND Items.Description LIKE \'%{tags[i]}%\' ";
                categoryTitleSql += $"AND Categories.Title LIKE \'%{tags[i]}%\' ";
                categoryDescriptionSql += $"AND Categories.Description LIKE \'%{tags[i]}%\' ";
            }
            sql += titleSql + descriptionSql + categoryTitleSql + categoryDescriptionSql;
            sql += $")) {orderString}";
            return sql;
        }

        public IActionResult Admin() => View();

        [HttpPost]
        public async Task<IActionResult> Admin(string id)
        {
            if ((id != adminKey) && (id != operatorKey))
            {
                ViewBag.Message = "Wrong key";
                return View();
            }
            string name = id == operatorKey ? "Operator" : "Admin";
            List<Claim> claims = new List<Claim> { new Claim(ClaimsIdentity.DefaultNameClaimType, name) };
            ClaimsIdentity identity = new ClaimsIdentity(claims, "ApplicationCookie"
                , ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(identity));
            return RedirectToAction("Index");
        }

        [Operator]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index");
        }
    }
}