﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Data.ViewModels;
using WebSite.Services.Interfaces;
using WebSite.Services.NonInjectable;

namespace WebSite.Controllers
{
    public class ItemsController : Controller
    {
        protected AppDbContext dbContext;
        protected int itemsPerPage = 8;
        protected int otherItemsCount = 4;
        protected IUpdateProcessor updateProcessor;

        public ItemsController(AppDbContext dbContext, IUpdateProcessor updateProcessor)
        {
            this.dbContext = dbContext;
            this.updateProcessor = updateProcessor;
        }

        public async Task<IActionResult> Index(int page = 1) =>
            View(await new PagesBuilder("/Items/Index?page={page}", page, itemsPerPage, true)
                .Build<Item, TwoClosestPagesGenerator>(dbContext.Items.OrderBy(c => c.Title)));

        public async Task<IActionResult> Details(int id, int page = 1)
        {
            Item item = await dbContext.Items.FirstOrDefaultAsync(i => i.Id == id);
            if (item == null)
                return NotFound();
            item.Category = await dbContext.Categories.FirstAsync(c => c.Id == item.CategoryId);
            return View(new OtherDetailsView<Item> { Model = item, Other = await dbContext.Items
                .Where(i => i.CategoryId == item.CategoryId).OrderBy(i => Guid.NewGuid()).Take(otherItemsCount)
                .ToListAsync()});
        }

        [Admin]
        public async Task<IActionResult> Edit(int id)
        {
            Item item = await dbContext.Items.FirstOrDefaultAsync(i => i.Id == id);
            if (item == null)
                return NotFound();
            return View(item);
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Edit(Item item, IFormFile image)
        {
            if (item.CategoryId == 0)
                ModelState.AddModelError(nameof(Item.CategoryId), "Category is not selected");
            if (!ModelState.IsValid)
                return View(item);
            await updateProcessor.UpdateTitled(item, image);
            return RedirectToAction("Edit", new { id = item.Id });
        }

        [Admin]
        public IActionResult Add(int categoryId = 0)
        {
            ViewBag.Adding = true;
            return View("Edit", new Item { CategoryId = categoryId });
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Add(Item item, IFormFile image)
        {
            ViewBag.Adding = true;
            if (item.CategoryId == 0)
                ModelState.AddModelError(nameof(Item.CategoryId), "Category is not selected");
            if (image == null)
                ModelState.AddModelError(nameof(Item.ImageUrl), "Image is required when adding");
            if (!ModelState.IsValid)
                return View("Edit", item);
            await updateProcessor.AddTitled(item, image);
            return RedirectToAction("Edit", new { id = item.Id });
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Remove(int id)
        {
            Item item = await dbContext.Items.FirstOrDefaultAsync(c => c.Id == id);
            if (item == null)
                return NotFound();
            await updateProcessor.RemoveTitled(item);
            return RedirectToAction("Index");
        }
    }
}
