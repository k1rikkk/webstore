﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Data.ViewModels;
using WebSite.Services.Interfaces;
using WebSite.Services.NonInjectable;

namespace WebSite.Controllers
{
    public class CartController : Controller
    {
        protected ICartProvider cartProvider;
        protected AppDbContext dbContext;

        public CartController(ICartProvider cartProvider, AppDbContext dbContext)
        {
            this.cartProvider = cartProvider;
            this.dbContext = dbContext;
        }

        public async Task<OkResult> Add(int itemId, int amount = 1)
        {
            await cartProvider.Add(itemId, amount);
            return Ok();
        }

        public async Task<IActionResult> Remove(int itemId)
        {
            await cartProvider.Remove(itemId);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CurrentSum() => new ObjectResult(await cartProvider.GetSum());

        public async Task<IActionResult> Index()
        {
            Cart cart = await cartProvider.GetCurrent();
            cart.Address = null;
            cart.PhoneNumber = null;
            return View(cart);
        }

        [HttpPost]
        public async Task<IActionResult> Index(Cart cart)
        {
            if (!ModelState.IsValid)
                return View(cart);
            cart.IsMade = true;
            cart.LastUpdate = DateTime.Now;
            cart.CartItems = null;
            dbContext.Update(cart);
            await dbContext.SaveChangesAsync();
            await cartProvider.Reset();
            return View("Thanks");
        }

        [Operator]
        public async Task<IActionResult> Browse(int reloadInterval = 0, bool autoReload = false
            , bool showDone = false, bool showUnprocessable = false)
        {
            reloadInterval = reloadInterval < 10 ? 10 : reloadInterval;
            return View(new CartsBrowserView
            {
                Carts = await dbContext.Carts.Where(c => (c.Unproccesable == showUnprocessable)
                    && (c.IsDone == showDone) && (c.IsMade == true)).ToListAsync(),
                AutoReload = autoReload,
                ReloadInterval = reloadInterval,
                ShowDone = showDone,
                ShowUnprocessable = showUnprocessable
            });
        }

        [Operator]
        public async Task<IActionResult> Details(int id)
        {
            Cart cart = await dbContext.Carts.Where(c => c.Id == id).Include(c => c.CartItems)
                .ThenInclude(ci => ci.Item).FirstOrDefaultAsync();
            if (cart == null)
                return NotFound();
            return View(cart);
        }

        [Operator]
        [HttpPost]
        public async Task<IActionResult> Details(int id, int status)
        {
            Cart cart = await dbContext.Carts.Where(c => c.Id == id).Include(c => c.CartItems)
                .ThenInclude(ci => ci.Item).FirstOrDefaultAsync();
            if (cart == null)
                return NotFound();
            bool isDone = status == 1;
            bool unprocessable = status == 2;
            if ((isDone == false) && (cart.IsDone == true) && (User.Identity.Name != "Admin"))
                return StatusCode(403);
            if ((unprocessable == false) && (cart.Unproccesable == true) && (User.Identity.Name != "Admin"))
                return StatusCode(403);
            cart.IsDone = isDone;
            cart.Unproccesable = unprocessable;
            dbContext.Update(cart);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Browse");
        }
    }
}
