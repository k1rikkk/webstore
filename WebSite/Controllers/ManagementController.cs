﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WebSite.Data.ViewModels;
using WebSite.Services.Interfaces;
using WebSite.Services.NonInjectable;

namespace WebSite.Controllers
{
    public class ManagementController : Controller
    {
        protected IAnalyst analyst;
        protected int topCategoriesCount = 7;
        protected int topItemsCount = 7;

        public ManagementController(IAnalyst analyst) => this.analyst = analyst;

        [Admin]
        public async Task<IActionResult> Statistics()
        {
            StatisticsView view = new StatisticsView(await analyst.GetTopItemsAsync(topItemsCount)
                , await analyst.GetTopCategoriesAsync(topCategoriesCount)
                , await analyst.GetMonthlyTotalCartsSum());
            return View(view);
        }

        public IActionResult Error() => View();

        public IActionResult Supply() => View();
    }
}