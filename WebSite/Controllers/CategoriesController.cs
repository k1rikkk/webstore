﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebSite.Data;
using WebSite.Data.Models;
using WebSite.Data.ViewModels;
using WebSite.Services.Interfaces;
using WebSite.Services.NonInjectable;
using WebSite.Services.NonInjectable.Interfaces;

namespace WebSite.Controllers
{
    public class CategoriesController : Controller
    {
        protected AppDbContext dbContext;
        protected int categoriesPerPage = 8;
        protected int itemsPerPage = 8;
        protected IUpdateProcessor updateProcessor;

        public CategoriesController(AppDbContext dbContext, IUpdateProcessor updateProcessor)
        {
            this.dbContext = dbContext;
            this.updateProcessor = updateProcessor;
        }

        public async Task<IActionResult> Index(int page = 1) => 
            View(await new PagesBuilder("/Categories/Index?page={page}", page, categoriesPerPage, true)
                .Build<Category, TwoClosestPagesGenerator>(dbContext.Categories.OrderBy(c => c.Title)));

        public async Task<IActionResult> Details(int id, int page = 1)
        {
            Category category = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == id);
            if (category == null)
                return NotFound();
            return View(new PagedDetailsView<Category, Item> { Model = category
                , Pages = await new PagesBuilder("/Categories/Details/" + category.Id + "?page={page}"
                + "#start", page, itemsPerPage, true)
                .Build<Item, TwoClosestPagesGenerator>(dbContext.Items.Where(i => i.CategoryId == category.Id))});
        }
        
        [Admin]
        public async Task<IActionResult> Edit(int id)
        {
            Category category = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == id);
            if (category == null)
                return NotFound();
            return View(category);
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Edit(Category category, IFormFile image)
        {
            if (!ModelState.IsValid)
                return View(category);
            await updateProcessor.UpdateTitled(category, image);
            return RedirectToAction("Edit", new { id = category.Id });
        }

        [Admin]
        public IActionResult Add()
        {
            ViewBag.Adding = true;
            return View("Edit", new Category());
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Add(Category category, IFormFile image)
        {
            ViewBag.Adding = true;
            if (image == null)
                ModelState.AddModelError(nameof(Category.ImageUrl), "Image is required when adding");
            if (!ModelState.IsValid)
                return View("Edit", category);
            await updateProcessor.AddTitled(category, image);
            return RedirectToAction("Edit", new { id = category.Id });
        }

        [Admin]
        [HttpPost]
        public async Task<IActionResult> Remove(int id)
        {
            Category category = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == id);
            if (category == null)
                return NotFound();
            await updateProcessor.RemoveTitled(category);
            return RedirectToAction("Index");
        }
    }
}
